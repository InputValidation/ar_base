Baseline Linux Machine
=========

Tested with Molecule on :
  - Centos 7
  - ubuntu 18.04
  - Ubuntu 19.04

Role Variables
--------------

```yaml
admin_name: sadmin
admin_username: sadmin
admin_shell: /bin/bash
admin_editor: vim
admin_gen_ssh_key: yes
admin_create_home: yes
admin_home_path: /home/sadmin
admin_comment: Admin User
pkgs:
  - vim
  - tree
  - etc...
ssh_port: 22
listen_address: 0.0.0.0
```

- Additionally an `admin_pass` Variable is required and must contain a hashed password -


Example Playbook
----------------

    - hosts: servers
      vars:
        admin_name: sadmin
        admin_username: sadmin
        admin_shell: /bin/bash
        admin_editor: vim
        admin_gen_ssh_key: yes
        admin_create_home: yes
        admin_home_path: /home/sadmin
        admin_comment: Admin User
        pkgs:
          - vim
          - tree
          - tmux
          - git
          - curl
          - wget
          - screen
          - zip
          - openssl
        ssh_port: 22
        listen_address: 0.0.0.0
      roles:
         - base

License
-------

BSD

Author Information
------------------

Hire Me


