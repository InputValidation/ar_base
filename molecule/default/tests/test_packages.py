import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


@pytest.mark.parametrize("pkgname", [
    ("tmux"),
    ("tree"),
])
def test_is_pkg_installed(host, pkgname):
    assert host.package(pkgname).is_installed
