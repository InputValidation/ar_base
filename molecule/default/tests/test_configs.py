import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_sudoers_file(host):
    s = host.file('/etc/sudoers.d/sadmin')
    assert s.exists
    assert s.contains('sadmin ALL=(ALL) ALL')


def test_sshd_config_file(host):
    sshd = host.file('/etc/ssh/sshd_config')
    assert sshd.exists
    assert sshd.contains('#Ansible Managed')


@pytest.mark.parametrize("file, content", [
    ("/home/sadmin/.bashrc", "#Ansible Managed"),
    ("/home/sadmin/.tmux.conf", "#Ansible Managed"),
    ("/home/sadmin/.vimrc", "\"Ansible Managed"),
    ("/home/sadmin/.ssh/config", "#Ansible Managed")
])
def test_config_files(host, file, content):
    conf = host.file(file)

    assert conf.exists
    assert conf.contains(content)
